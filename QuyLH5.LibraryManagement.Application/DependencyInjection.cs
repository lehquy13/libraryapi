﻿using Microsoft.Extensions.DependencyInjection;
using QuyLH5.LibraryManagement.Application.Contracts.Interfaces;
using QuyLH5.LibraryManagement.Application.Mapping;
using QuyLH5.LibraryManagement.Application.ServiceImpls;
using QuyLH5.LibraryManagement.Domain.DomainServices;
using QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;

namespace QuyLH5.LibraryManagement.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddApplicationMappings();
            
            //Domain services
            services.AddTransient<IBasketDomainServices, BasketDomainServices>();
            services.AddTransient<IIdentityDomainServices, IdentityDomainServices>();
            services.AddTransient<IOrderDomainServices, OrderDomainServices>();
            services.AddTransient<IBookDomainServices, BookDomainServices>();
            services.AddTransient<IWishListDomainServices, WishListDomainServices>();

            //Application services
            services.AddScoped<IAuthenticationServices, AuthenticationServices>();
            services.AddScoped<IBasketServices, BasketServices>();
            services.AddScoped<IUserServices, UserServices>();
            services.AddScoped<IOrderServices, OrderServices>();
            services.AddScoped<IBookServices, BookServices>();
            services.AddScoped<IWishListServices, WishListServices>();

            return services;
        }
    }
}