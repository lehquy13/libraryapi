﻿using QuyLH5.LibraryManagement.Application.Contracts.Commons.Primitives;

namespace QuyLH5.LibraryManagement.Application.Contracts.Baskets;

public class BasketForDetailDto : EntityDto<int>
{
    public List<BasketItemForList> Items { get; set; } = new();

    public decimal TotalPrice { get; set; } = 0;

    public override string ToString()
    {
        return $"Basket has: {Items.Count} items";
    }
}