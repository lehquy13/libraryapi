﻿using QuyLH5.LibraryManagement.Application.Contracts.Commons.Primitives;

namespace QuyLH5.LibraryManagement.Application.Contracts.Books.BookDtos;

public class AuthorDto: EntityDto<int>
{
    public string Name { get; set; } = string.Empty;

    public override string ToString()
    {
        return $"Author with Id: {Id} Name: {Name}";
    }
}