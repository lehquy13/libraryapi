﻿using QuyLH5.LibraryManagement.Domain.Shared.Paginations;

namespace QuyLH5.LibraryManagement.Application.Contracts.Order;

public class OrderPaginatedParams : PaginatedParams
{
   public Guid UserId { get; set; } = Guid.Empty;
}