﻿namespace QuyLH5.LibraryManagement.Application.Contracts.Authentications;

public record ValidateTokenQuery
(
    string ValidateToken
);

