﻿using QuyLH5.LibraryManagement.Application.Contracts.Users;
using QuyLH5.LibraryManagement.Domain.Shared.Results;

namespace QuyLH5.LibraryManagement.Application.Contracts.Interfaces;

public interface IWishListServices
{
    Task<Result> AddItemToWishList(WishlistItemForAddRemoveDto wishlistItemForAddRemoveDto);
    Task<Result> RemoveItemFromWishList(WishlistItemForAddRemoveDto wishlistItemForAddRemoveDto);
    
    Task<Result> AddFavouriteBook(WishlistItemForAddRemoveDto wishlistItemForAddRemoveDto);
    Task<Result> RemoveFavouriteBook(WishlistItemForAddRemoveDto wishlistItemForAddRemoveDto);
}