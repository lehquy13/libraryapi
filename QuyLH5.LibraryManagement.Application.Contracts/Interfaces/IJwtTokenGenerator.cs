﻿using QuyLH5.LibraryManagement.Application.Contracts.Authentications;

namespace QuyLH5.LibraryManagement.Application.Contracts.Interfaces
{
    public interface IJwtTokenGenerator
    {
        string GenerateToken(UserForLoginResponseDto userForLoginResponseDto);
        bool ValidateToken(string token);
    }
}
