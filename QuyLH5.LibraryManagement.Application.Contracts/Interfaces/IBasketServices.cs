﻿using QuyLH5.LibraryManagement.Application.Contracts.Baskets;
using QuyLH5.LibraryManagement.Domain.Shared.Results;

namespace QuyLH5.LibraryManagement.Application.Contracts.Interfaces;

public interface IBasketServices
{
    Task<Result<BasketForDetailDto>> GetBasketAsync(Guid userId);
    
    Task<Result> AddItemToBasket(AddItemToBasketCommand addItemToBasketCommand);
    
    Task<Result> DeleteBasketAsync(int basketId);
    
    Task<Result> SetQuantities(SetQuantitiesCommand setQuantitiesCommand);
}