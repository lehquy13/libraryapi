﻿using QuyLH5.LibraryManagement.Application.Contracts.Commons.Primitives;

namespace QuyLH5.LibraryManagement.Application.Contracts.Users;

public class UserForListDto : EntityDto<Guid>
{
    public string Name { get; set; } = string.Empty;

    public override string ToString()
    {
        return $"User with Id: {Id}, Name: {Name}";
    }
}