﻿namespace QuyLH5.LibraryManagement.Application.Contracts.Users;

public record WishlistItemForAddRemoveDto(Guid UserId, int BookId);
