﻿namespace QuyLH5.LibraryManagement.Domain.Shared.Enums;

public enum PaymentMethod
{
    Cash,
    Card,
    Default = Cash
}