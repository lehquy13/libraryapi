﻿using QuyLH5.LibraryManagement.Domain.Library.UserAggregate;

namespace QuyLH5.LibraryManagement.Domain.Specifications.Users;

public sealed class UserListQuerySpec : GetListSpecificationBase<User>
{
    public UserListQuerySpec(
        int pageIndex,
        int pageSize)
        : base(pageIndex, pageSize)
    {
    }
}