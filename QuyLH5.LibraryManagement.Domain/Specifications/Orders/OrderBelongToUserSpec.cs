﻿using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;

namespace QuyLH5.LibraryManagement.Domain.Specifications.Orders;

public class OrderBelongToUserSpec : SpecificationBase<Order>
{
    public OrderBelongToUserSpec(OrderGuid orderId, IdentityGuid userId)
    {
        Criteria = order => 
            order.Id == orderId && order.UserId == userId;
        
        AddInclude(ord => ord.User);
        AddInclude(ord => ord.OrderItems);
        AddInclude("OrderItems.Book");
    }
}