﻿using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Enums;

namespace QuyLH5.LibraryManagement.Domain.Specifications.Orders;

public class OrderHavingBookByIdSpec : SpecificationBase<Order>
{
    public OrderHavingBookByIdSpec(int bookId, IdentityGuid userId)
    {
        Criteria = order =>
            order.OrderItems.Any(x => x.BookId == bookId) &&
            order.UserId == userId &&
            order.OrderStatus == OrderStatus.Completed;
    }
}