﻿using QuyLH5.LibraryManagement.Domain.Library.BookAggregate;

namespace QuyLH5.LibraryManagement.Domain.Specifications.Books;

public class AuthorListByIdQuerySpec : SpecificationBase<Author>
{
    public AuthorListByIdQuerySpec(List<int> ints)
    {
        Criteria = author =>
            ints.Contains(author.Id);
    }
}