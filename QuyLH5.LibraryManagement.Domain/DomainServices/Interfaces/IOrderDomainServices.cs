﻿using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Enums;
using QuyLH5.LibraryManagement.Domain.Shared.Results;

namespace QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;

public interface IOrderDomainServices
{
    Task<Result<Order>> CreateOrderAsync(IdentityGuid customerId);
    
    Task<Result<Order>> PurchaseOrder(IdentityGuid customerId, OrderGuid orderId, PaymentMethod paymentMethod);
}