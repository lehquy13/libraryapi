﻿using QuyLH5.LibraryManagement.Domain.Library.BasketAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Results;

namespace QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;

public interface IBasketDomainServices
{
    Task<Result> AddItemToBasket(IdentityGuid id, int bookId, decimal price, int quantity = 1);

    Task DeleteBasketAsync(int basketId);

    Task<Result<Basket>> SetQuantities(IdentityGuid identityGuid, Dictionary<int, int> quantities);
}