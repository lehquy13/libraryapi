﻿using QuyLH5.LibraryManagement.Domain.DomainServices.Exceptions;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.BasketAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.Identity;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Results;

namespace QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;

public interface IIdentityDomainServices
{
    Task<IdentityUser?> SignInAsync(string email, string password);

    Task<IdentityUser?> FindByEmailAsync(string email);
    
    Task<IdentityUser?> GetUserIdAsync(IdentityGuid id);

    Task<Result<IdentityUser>> CreateAsync(
        string email,
        string password,
        string phoneNumber,
        string name,
        string city,
        string country);

    Task<Result> ChangePassword(IdentityGuid identityId, string currentPassword, string newPassword);
}