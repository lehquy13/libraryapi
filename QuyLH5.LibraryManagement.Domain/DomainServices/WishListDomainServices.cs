﻿using QuyLH5.LibraryManagement.Domain.DomainServices.Exceptions;
using QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.BookAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Results;

namespace QuyLH5.LibraryManagement.Domain.DomainServices;

public class WishListDomainServices : IWishListDomainServices
{
    private readonly IUserRepository _userRepository;
    private readonly IBookRepository _bookRepository;
    private readonly IAppLogger<WishListDomainServices> _logger;

    public WishListDomainServices(IUserRepository userRepository, IAppLogger<WishListDomainServices> logger,
        IBookRepository bookRepository)
    {
        _userRepository = userRepository;
        _logger = logger;
        _bookRepository = bookRepository;
    }

   
}