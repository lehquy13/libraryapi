﻿using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;

namespace QuyLH5.LibraryManagement.Domain.Library.BasketAggregate;

public interface IBasketRepository : IRepository<Basket, int>
{
    Task<Basket?> GetBasketByUserIdAsync(IdentityGuid userId);
}