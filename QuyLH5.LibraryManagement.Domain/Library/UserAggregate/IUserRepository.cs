﻿using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;

namespace QuyLH5.LibraryManagement.Domain.Library.UserAggregate;

public interface IUserRepository : IRepository<User, IdentityGuid>
{
    Task<User?> GetFullById(IdentityGuid id);
}