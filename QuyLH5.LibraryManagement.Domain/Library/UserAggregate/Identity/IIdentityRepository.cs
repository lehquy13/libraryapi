﻿using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;

namespace QuyLH5.LibraryManagement.Domain.Library.UserAggregate.Identity;

public interface IIdentityRepository : IRepository<IdentityUser, IdentityGuid>
{
    Task<IdentityUser?> FindByEmailAsync(string email);
}