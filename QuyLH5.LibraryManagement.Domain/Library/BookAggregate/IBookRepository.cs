﻿using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Specifications.Books;

namespace QuyLH5.LibraryManagement.Domain.Library.BookAggregate;

public interface IBookRepository : IRepository<Book, int>
{
    Task<List<Review>> GetBookReviews(int bookId);
    Task<List<Author>> GetAuthors(AuthorListByIdQuerySpec authorListByIdQuerySpec);
}