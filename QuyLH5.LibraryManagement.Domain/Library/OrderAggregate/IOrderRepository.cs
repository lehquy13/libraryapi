﻿using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Specifications.Orders;

namespace QuyLH5.LibraryManagement.Domain.Library.OrderAggregate;

public interface IOrderRepository : IRepository<Order,OrderGuid>
{
    Task<List<Order>> GetAllListAsync(OrderListQuerySpec orderListQuerySpec);
}