﻿using QuyLH5.LibraryManagement.Domain.Interfaces;

namespace QuyLH5.LibraryManagement.Domain.Primitives;

public interface IAggregateRoot<TId> : IEntity<TId> where TId : notnull
{
}