﻿using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace QuyLH5.LibraryManagement.WebApi.Controllers;

[Authorize]
public class AuthorizeApiController : ApiController
{
    public AuthorizeApiController(IMapper mapper, ILogger<AuthorizeApiController> logger) : base(mapper, logger)
    {
    }
}