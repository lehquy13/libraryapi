﻿using FluentValidation;
using QuyLH5.LibraryManagement.WebApi.Models;

namespace QuyLH5.LibraryManagement.WebApi.Validations;

public class DepositRequestValidator : AbstractValidator<DepositRequest>
{
    public DepositRequestValidator()
    {
        RuleFor(x => x.Amount).GreaterThan(0);
    }
}
