using MapsterMapper;
using Moq;
using QuyLH5.LibraryManagement.Application.Contracts.Baskets;
using QuyLH5.LibraryManagement.Application.Contracts.Commons.ErrorMessages;
using QuyLH5.LibraryManagement.Application.ServiceImpls;
using QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.BasketAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Results;
using QuyLH5.LibraryManagement.Test.Shared;

namespace QuyLH5.LibraryManagement.Application.Tests;

public class ApplicationBasketServicesTests
{
    private readonly Mock<IBasketRepository> _basketRepositoryMock = new();
    private readonly Mock<IBasketDomainServices> _basketDomainServicesMock = new();
    private readonly Mock<IMapper> _mapperMock = new();
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new();
    private readonly Mock<IAppLogger<BasketServices>> _loggerMock = new();

    private BasketServices _basketServices;

    public ApplicationBasketServicesTests()
    {
        _basketServices = new BasketServices(
            _mapperMock.Object,
            _unitOfWorkMock.Object,
            _loggerMock.Object,
            _basketDomainServicesMock.Object,
            _basketRepositoryMock.Object
        );
    }

    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task GetBasketAsync_Should_Return_Basket_For_Existing_User()
    {
        // Arrange
        var expectedBasket = TestShared.Baskets[0]; // Replace with your actual BasketEntity

        _basketRepositoryMock.Setup(repo => repo.GetBasketByUserIdAsync(It.IsAny<IdentityGuid>()))
            .ReturnsAsync(expectedBasket);
        _mapperMock.Setup(mapper => mapper.Map<BasketForDetailDto>(It.IsAny<Basket>()))
            .Returns(new BasketForDetailDto()); // Replace with your actual BasketForDetailDto

        // Act
        var result = await _basketServices.GetBasketAsync(new Guid());

        // Assert
        Assert.NotNull(result);
        Assert.True(result.IsSuccess);
        Assert.IsInstanceOf<Result<BasketForDetailDto>>(result);
        Assert.NotNull(result.Value);
    }

    [Test]
    public async Task GetBasketAsync_ReturnsFailure_WhenNotFound()
    {
        // Arrange
        _basketRepositoryMock.Setup(repo => repo.GetBasketByUserIdAsync(It.IsAny<IdentityGuid>()))
            .ReturnsAsync((Basket)null); // Simulate not found scenario

        // Act
        var result = await _basketServices.GetBasketAsync(new Guid());

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BasketErrorMessages.BasketNotFound));
    }

    [Test]
    public async Task GetBasketAsync_ReturnsFailure_WhenMapperThrowsException()
    {
        // Arrange
        var expectedBasket = TestShared.Baskets[0]; // Replace with your actual BasketEntity
        var exceptionMessage = "Exception message";

        _basketRepositoryMock.Setup(repo => repo.GetBasketByUserIdAsync(It.IsAny<IdentityGuid>()))
            .ReturnsAsync(expectedBasket);
        _mapperMock.Setup(mapper => mapper.Map<BasketForDetailDto>(It.IsAny<Basket>()))
            .Throws(new Exception(exceptionMessage)); // Simulate repository exception

        // Act
        var result = await _basketServices.GetBasketAsync(new Guid());

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(exceptionMessage, Is.EqualTo(result.DisplayMessage));
    }

    [Test]
    public async Task GetBasketAsync_ReturnsFailure_WhenRepositoryThrowsException()
    {
        // Arrange
        var exceptionMessage = "Exception message";

        _basketRepositoryMock.Setup(repo => repo.GetBasketByUserIdAsync(It.IsAny<IdentityGuid>()))
            .ThrowsAsync(new Exception(exceptionMessage)); // Simulate repository exception

        // Act
        var result = await _basketServices.GetBasketAsync(new Guid());

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(exceptionMessage, Is.EqualTo(result.DisplayMessage));
    }

    //AddItemToBasket

    [Test]
    public async Task AddItemToBasket_ReturnsFailure_WhenAddItemToBasketDomainServiceFails()
    {
        // Arrange
        var addItemToBasketCommand = new AddItemToBasketCommand(Guid.NewGuid(), 1, 10.0m, 2);

        _basketDomainServicesMock.Setup(services =>
                services.AddItemToBasket(
                    It.IsAny<IdentityGuid>(),
                    It.IsAny<int>(),
                    It.IsAny<decimal>(),
                    It.IsAny<int>()))
            .ReturnsAsync(Result.Fail("Error adding item to basket"));

        // Act
        var result = await _basketServices.AddItemToBasket(addItemToBasketCommand);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.IsNotEmpty(result.DisplayMessage);
    }

    [Test]
    public async Task AddItemToBasket_ReturnsFailure_WhenSaveChangesFails()
    {
        // Arrange
        var addItemToBasketCommand = new AddItemToBasketCommand(Guid.NewGuid(), 1, 10.0m, 2);

        _basketDomainServicesMock.Setup(services =>
                services.AddItemToBasket(
                    It.IsAny<IdentityGuid>(),
                    It.IsAny<int>(),
                    It.IsAny<decimal>(),
                    It.IsAny<int>()))
            .ReturnsAsync(Result.Success());


        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulating SaveChanges failure

        // Act
        var result = await _basketServices.AddItemToBasket(addItemToBasketCommand);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BasketErrorMessages.AddItemFailWhileSavingChanges));
        // Add more assertions based on your implementation
    }

    [Test]
    public async Task AddItemToBasket_ReturnsSuccess_WhenAddItemToBasketAndSaveChangesSucceeds()
    {
        // Arrange
        var addItemToBasketCommand = new AddItemToBasketCommand(Guid.NewGuid(), 1, 10.0m, 2);

        _basketDomainServicesMock.Setup(services =>
                services.AddItemToBasket(
                    It.IsAny<IdentityGuid>(),
                    It.IsAny<int>(),
                    It.IsAny<decimal>(),
                    It.IsAny<int>()))
            .ReturnsAsync(Result.Success());

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulating SaveChanges success

        // Act
        var result = await _basketServices.AddItemToBasket(addItemToBasketCommand);

        // Assert
        Assert.True(result.IsSuccess);
    }

    //Delete basket
    [Test]
    public async Task DeleteBasketAsync_ReturnsFailure_WhenSaveChangesFails()
    {
        // Arrange
        var basketId = 1;
        _basketDomainServicesMock.Setup(services =>
                services.DeleteBasketAsync(It.IsAny<int>()))
            .Returns(Task.CompletedTask); // Simulating DeleteBasketAsync success

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulating SaveChanges failure


        // Act
        var result = await _basketServices.DeleteBasketAsync(basketId);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BasketErrorMessages.AddItemFailWhileSavingChanges));
        // Add more assertions based on your implementation
    }

    [Test]
    public async Task DeleteBasketAsync_ReturnsSuccess_WhenDeleteBasketAndSaveChangesSucceeds()
    {
        // Arrange
        var basketId = 1;

        _basketDomainServicesMock.Setup(services =>
                services.DeleteBasketAsync(It.IsAny<int>()))
            .Returns(Task.CompletedTask); // Simulating DeleteBasketAsync success

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulating SaveChanges success

        // Act
        var result = await _basketServices.DeleteBasketAsync(basketId);

        // Assert
        Assert.True(result.IsSuccess);
    }

    //SetQuantities
    [Test]
    public async Task SetQuantities_ReturnsFailure_WhenSetQuantitiesDomainServiceFails()
    {
        // Arrange
        var userId = Guid.NewGuid();
        var quantities = new Dictionary<int, int> { { 1, 5 }, { 2, 3 } };

        _basketDomainServicesMock.Setup(services =>
                services.SetQuantities(It.IsAny<IdentityGuid>(), It.IsAny<Dictionary<int, int>>()))
            .ReturnsAsync(Result.Fail("Error setting quantities"));

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulating SaveChanges success

        // Act
        var result = await _basketServices.SetQuantities(new SetQuantitiesCommand(userId, quantities));

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Greater(result.ErrorMessages.Count, 0);
        Assert.That(result.DisplayMessage, Is.EqualTo(BasketErrorMessages.SetQuantitiesFail));
        // Add more assertions based on your implementation
    }

    [Test]
    public async Task SetQuantities_ReturnsFailure_WhenSaveChangesFails()
    {
        // Arrange
        var userId = Guid.NewGuid();
        var quantities = new Dictionary<int, int> { { 1, 5 }, { 2, 3 } };

        _basketDomainServicesMock.Setup(services =>
                services.SetQuantities(It.IsAny<IdentityGuid>(), It.IsAny<Dictionary<int, int>>()))
            .ReturnsAsync(Result.Success()); // Simulating SetQuantities success

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulating SaveChanges failure

        // Act
        var result = await _basketServices.SetQuantities(new SetQuantitiesCommand(userId, quantities));

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BasketErrorMessages.SetQuantitiesFailWhileSavingChanges));
        // Add more assertions based on your implementation
    }

    [Test]
    public async Task SetQuantities_ReturnsSuccess_WhenSetQuantitiesAndSaveChangesSucceeds()
    {
        // Arrange
        var userId = Guid.NewGuid();
        var quantities = new Dictionary<int, int> { { 1, 5 }, { 2, 3 } };

        _basketDomainServicesMock.Setup(services =>
                services.SetQuantities(It.IsAny<IdentityGuid>(), It.IsAny<Dictionary<int, int>>()))
            .ReturnsAsync(Result.Success()); // Simulating SetQuantities success

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulating SaveChanges success

        // Act
        var result = await _basketServices.SetQuantities(new SetQuantitiesCommand(userId, quantities));

        // Assert
        Assert.True(result.IsSuccess);
        // Add more assertions based on your implementation
    }
}