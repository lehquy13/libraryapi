using MapsterMapper;
using MockQueryable.Moq;
using Moq;
using QuyLH5.LibraryManagement.Application.Contracts.Books;
using QuyLH5.LibraryManagement.Application.Contracts.Books.BookDtos;
using QuyLH5.LibraryManagement.Application.Contracts.Books.ReviewDtos;
using QuyLH5.LibraryManagement.Application.Contracts.Commons.ErrorMessages;
using QuyLH5.LibraryManagement.Application.Contracts.Users;
using QuyLH5.LibraryManagement.Application.ServiceImpls;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.BookAggregate;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Enums;
using QuyLH5.LibraryManagement.Domain.Shared.Params;
using QuyLH5.LibraryManagement.Domain.Specifications.Books;
using QuyLH5.LibraryManagement.Domain.Specifications.Orders;
using QuyLH5.LibraryManagement.Test.Shared;

namespace QuyLH5.LibraryManagement.Application.Tests;

public class ApplicationBookServicesTests
{
    private readonly Mock<IBookRepository> _bookRepositoryMock = new();
    private readonly Mock<IOrderRepository> _orderRepositoryMock = new();
    private readonly Mock<IUserRepository> _userRepositoryMock = new();
    private readonly Mock<IMapper> _mapperMock = new();
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new();
    private readonly Mock<IAppLogger<BookServices>> _loggerMock = new();

    private BookServices _bookServices;

    public ApplicationBookServicesTests()
    {
        _bookServices = new BookServices(
            _unitOfWorkMock.Object,
            _mapperMock.Object,
            _loggerMock.Object,
            _bookRepositoryMock.Object,
            _orderRepositoryMock.Object,
            _userRepositoryMock.Object
        );
    }

    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task GetBooks_WithValidFilter_ShouldReturnPagedResult()
    {
        // Arrange
        var bookFilterParams = new BookFilterParams
        {
            // Set your filter parameters here
        };

        var expectedBooks = TestShared.Books;
        var expectedBookDtos = TestShared.BookForListDtos;

        //2 - build mock by extension
        var mock = expectedBooks.AsQueryable().BuildMock();
        _bookRepositoryMock.Setup(repo => repo.GetAll())
            .Returns(mock);

        _bookRepositoryMock.Setup(repo => repo.CountAsync())
            .ReturnsAsync(expectedBooks.Count);

        _mapperMock.Setup(ma => ma.Map<List<BookForListDto>>(expectedBooks))
            .Returns(expectedBookDtos);

        // Act
        var result = await _bookServices.GetBooksAsync(bookFilterParams);

        // Assert
        Assert.IsNotNull(result);
        // Add assertions for the result
    }

    [Test]
    public async Task GetBookByIdAsync_WithValidId_ShouldReturnBookForDetailDto()
    {
        // Arrange
        var expectedBook = TestShared.Books[0];
        var expectedBookDto = TestShared.BookForDetailDtos[0];

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(expectedBook.Id))
            .ReturnsAsync(expectedBook);

        _mapperMock.Setup(ma => ma.Map<BookForDetailDto>(expectedBook))
            .Returns(expectedBookDto);

        // Act
        var result = await _bookServices.GetBookByIdAsync(expectedBook.Id);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
        Assert.That(expectedBookDto.Title, Is.EqualTo(result.Value.Title));
    }

    [Test]
    public async Task GetBookByIdAsync_WithInvalidId_ShouldReturnNotFoundResult()
    {
        // Arrange
        Book? expectedBook = null;

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(9999))
            .ReturnsAsync(expectedBook);

        // Act
        var result = await _bookServices.GetBookByIdAsync(9999);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
    }

    [Test]
    public async Task UpsertBookAsync_WithNewBook_ShouldCreateAndReturnSuccess()
    {
        //Arrange
        Book? nullBook = null;
        List<Author> authors = new List<Author>()
        {
            TestShared.Authors[0]
        };
        var newBookUpsert = new BookForUpsertDto()
        {
            AuthorIds = new List<int>()
            {
                TestShared.Authors[0].Id
            },
            Title = "New Book",
            Quantity = 1,
            ImageUrl = "image",
            Price = 100,
            CurrentPrice = 90,
            Genre = Genre.Adventure.ToString(),
            PublicationDate = DateTime.Now
        };
        var expectedBook = Book.Create("New Book", 1, "image", 100, 90,
            authors, Genre.Adventure, DateTime.Now);

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(newBookUpsert.Id))
            .ReturnsAsync(nullBook);

        _bookRepositoryMock.Setup(repo => repo.GetAuthors(It.IsAny<AuthorListByIdQuerySpec>()))
            .ReturnsAsync(authors);

        //_mapperMock.Setup(ma => ma.Map<Book>(newBookUpsert)).Returns(expectedBook);

        _bookRepositoryMock.Setup(repo => repo.InsertAsync(expectedBook));

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        //Act
        var result = await _bookServices.UpsertBookAsync(newBookUpsert);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
    }

    [Test]
    public async Task UpsertBookAsync_ShouldReturnFail_WhenSavingChangesFail()
    {
        //Arrange
        Book? nullBook = null;
        List<Author> authors = new List<Author>()
        {
            TestShared.Authors[0]
        };
        var newBookUpsert = new BookForUpsertDto()
        {
            AuthorIds = new List<int>()
            {
                TestShared.Authors[0].Id
            },
            Title = "New Book",
            Quantity = 1,
            ImageUrl = "image",
            Price = 100,
            CurrentPrice = 90,
            Genre = Genre.Adventure.ToString(),
            PublicationDate = DateTime.Now
        };
        var expectedBook = Book.Create("New Book", 1, "image", 100, 90,
            authors, Genre.Adventure, DateTime.Now);

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(newBookUpsert.Id))
            .ReturnsAsync(nullBook);

        _bookRepositoryMock.Setup(repo => repo.GetAuthors(It.IsAny<AuthorListByIdQuerySpec>()))
            .ReturnsAsync(authors);

        _bookRepositoryMock.Setup(repo => repo.InsertAsync(expectedBook));

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0);

        //Act
        var result = await _bookServices.UpsertBookAsync(newBookUpsert);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BookErrorMessages.UpsertFailWhileSavingChanges));
    }

    [Test]
    public async Task DeleteBookAsync_ShouldReturnFail_WhenSavingChangesFail()
    {
        var expectedBook = TestShared.Books[0];

        _bookRepositoryMock.Setup(repo => repo.DeleteByIdAsync(expectedBook.Id))
            .ReturnsAsync(true);

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0);

        //Act
        var result = await _bookServices.DeleteBookAsync(expectedBook.Id);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BookErrorMessages.DeleteFailWhileSavingChanges));
    }

    [Test]
    public async Task UpsertBookAsync_WithExistingBook_ShouldUpdateAndReturnSuccess()
    {
        //Arrange
        var bookToUpdate = new BookForUpsertDto();
        string image = "Image of Good Book";

        var expectedBook = Book.Create("Update Book", 1, image, 100, 90,
            TestShared.Authors, Genre.Adventure, DateTime.Now);

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(bookToUpdate.Id))
            .ReturnsAsync(expectedBook);

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        _bookRepositoryMock.Setup(br => br.GetAuthors(It.IsAny<AuthorListByIdQuerySpec>()))
            .ReturnsAsync(expectedBook.Authors);

        //Act
        var result = await _bookServices.UpsertBookAsync(bookToUpdate);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
    }

    [Test]
    public async Task DeleteBookAsync_WithValidId_ShouldDeleteAndReturnSuccess()
    {
        var expectedBook = TestShared.Books[0];

        _bookRepositoryMock.Setup(repo => repo.DeleteByIdAsync(expectedBook.Id))
            .ReturnsAsync(true);

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        //Act
        var result = await _bookServices.DeleteBookAsync(expectedBook.Id);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
    }

    [Test]
    public async Task DeleteBookAsync_WithInvalidId_ShouldReturnNotFoundResult()
    {
        var expectedBookId = 999;
        Book? book = null;

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(expectedBookId))
            .ReturnsAsync(book);

        //Act
        var result = await _bookServices.DeleteBookAsync(expectedBookId);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
    }

    [Test]
    public async Task GetReviews_WithValidBookId_ReturnsListOfReviews()
    {
        // Arrange
        int bookId = 1;
        var mockBook = TestShared.Books[0];
        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(bookId)).ReturnsAsync(mockBook);
        _mapperMock.Setup(ma => ma.Map<List<ReviewForListDto>>(mockBook.Reviews))
            .Returns(new List<ReviewForListDto>());
        // Act
        var result = await _bookServices.GetReviews(bookId);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
        Assert.IsInstanceOf<List<ReviewForListDto>>(result.Value);
    }

    [Test]
    public async Task GetReviews_ReturnsListOfReviews_WhenBookNotFound()
    {
        // Arrange
        int bookId = 1;
        Book? mockBook = null;
        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(bookId)).ReturnsAsync(mockBook);

        // Act
        var result = await _bookServices.GetReviews(bookId);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BookErrorMessages.BookNotFound));
    }

    [Test]
    public async Task AddReviewAsync_ReturnsListOfReviews_WhenBookNotFound()
    {
        // Arrange
        Book? mockBook = TestShared.Books[1];
        var customer = TestShared.Users[0];
        var reviewForCreateDto = new ReviewForCreateDto("test", "content with long conetnt", "image", mockBook.Id,
            customer.Id.Value, true, 5); // Create a mock ReviewForCreateDto object for testing

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(reviewForCreateDto.BookId)).ReturnsAsync((Book)null);

        // Act
        var result = await _bookServices.AddReviewAsync(reviewForCreateDto);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BookErrorMessages.BookNotFound));
    }

    [Test]
    public async Task AddReviewAsync_ReturnsListOfReviews_WhenUserNotFound()
    {
        // Arrange
        var mockBook = TestShared.Books[1];
        User? customer = TestShared.Users[0];
        var orders = TestShared.Orders;
        var reviewForCreateDto = new ReviewForCreateDto("test", "content with long conetnt", "image", mockBook.Id,
            customer.Id.Value, true, 5); // Create a mock ReviewForCreateDto object for testing

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(reviewForCreateDto.BookId)).ReturnsAsync(mockBook);
        _userRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<IdentityGuid>())).ReturnsAsync((User)null);
        _orderRepositoryMock.Setup(repo => repo.GetAllListAsync(It.IsAny<OrderHavingBookByIdSpec>()))
            .ReturnsAsync(orders);
        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        // Act
        var result = await _bookServices.AddReviewAsync(reviewForCreateDto);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(UserErrorMessages.UserNotFound));
    }

    [Test]
    public async Task AddReviewAsync_ReturnsListOfReviews_OrderCountIs0()
    {
        // Arrange
        var mockBook = TestShared.Books[1];
        User? customer = TestShared.Users[0];
        var orders = new List<Order>() { };
        var reviewForCreateDto = new ReviewForCreateDto("test", "content with long conetnt", "image", mockBook.Id,
            customer.Id.Value, true, 5); // Create a mock ReviewForCreateDto object for testing

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(reviewForCreateDto.BookId)).ReturnsAsync(mockBook);
        _userRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<IdentityGuid>())).ReturnsAsync(customer);
        _orderRepositoryMock.Setup(repo => repo.GetAllListAsync(It.IsAny<OrderHavingBookByIdSpec>()))
            .ReturnsAsync(orders);
        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        // Act
        var result = await _bookServices.AddReviewAsync(reviewForCreateDto);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(BookErrorMessages.UserMustBuyBookBeforeReviewing));
    }

    [Test]
    public async Task AddReviewAsync_WithValidReview_ReturnsSuccess()
    {
        // Arrange
        var mockBook = TestShared.Books[1];
        var customer = TestShared.Users[0];
        var orders = TestShared.Orders;
        var reviewForCreateDto = new ReviewForCreateDto("test", "content with long conetnt", "image", mockBook.Id,
            customer.Id.Value, true, 5); // Create a mock ReviewForCreateDto object for testing

        _bookRepositoryMock.Setup(repo => repo.GetByIdAsync(reviewForCreateDto.BookId)).ReturnsAsync(mockBook);
        _userRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<IdentityGuid>())).ReturnsAsync(customer);
        _orderRepositoryMock.Setup(repo => repo.GetAllListAsync(It.IsAny<OrderHavingBookByIdSpec>()))
            .ReturnsAsync(orders);
        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        // Act
        var result = await _bookServices.AddReviewAsync(reviewForCreateDto);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
    }
}