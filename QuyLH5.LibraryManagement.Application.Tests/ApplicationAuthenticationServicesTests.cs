using MapsterMapper;
using Moq;
using QuyLH5.LibraryManagement.Application.Contracts.Authentications;
using QuyLH5.LibraryManagement.Application.Contracts.Commons.ErrorMessages;
using QuyLH5.LibraryManagement.Application.Contracts.Interfaces;
using QuyLH5.LibraryManagement.Application.ServiceImpls;
using QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.Identity;
using QuyLH5.LibraryManagement.Domain.Shared.Results;

namespace QuyLH5.LibraryManagement.Application.Tests;

public class ApplicationAuthenticationServicesTests
{
    private readonly Mock<IJwtTokenGenerator> _jwtTokenGeneratorMock = new();
    private readonly Mock<IEmailSender> _emailSenderMock = new();
    private readonly Mock<IIdentityDomainServices> _identityDomainServicesMock = new();
    private readonly Mock<IMapper> _mapperMock = new();
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new();
    private readonly Mock<IAppLogger<AuthenticationServices>> _loggerMock = new();

    private AuthenticationServices _authenticationServices;

    public ApplicationAuthenticationServicesTests()
    {
        _authenticationServices = new AuthenticationServices(
            _mapperMock.Object,
            _unitOfWorkMock.Object,
            _loggerMock.Object,
            _identityDomainServicesMock.Object,
            _jwtTokenGeneratorMock.Object,
            _emailSenderMock.Object
        );
    }

    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task Login_ReturnsFailure_WhenSignInAsyncReturnsNull()
    {
        // Arrange
        var loginQuery = new LoginQuery("test@example.com", "testpassword");

        _identityDomainServicesMock.Setup(services =>
                services.SignInAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync((IdentityUser)null); // Simulating SignInAsync returning null

        _jwtTokenGeneratorMock.Setup(generator =>
                generator.GenerateToken(It.IsAny<UserForLoginResponseDto>()))
            .Returns("token"); // Simulating successful token generation

        // Act
        var result = await _authenticationServices.Login(loginQuery);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(AuthenticationErrorMessages.LoginFail));
        Assert.Null(result.Value); // Check if value is null when login fails
    }

    [Test]
    public async Task Login_ReturnsSuccess_WhenSignInAsyncReturnsIdentityUser()
    {
        // Arrange
        var loginQuery = new LoginQuery("test@example.com", "testpassword");
        var mockIdentityUser = new IdentityUser("test@example.com", "testpassword");
        var expectedToken = "token";
        _identityDomainServicesMock.Setup(services =>
                services.SignInAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(mockIdentityUser); // Simulating SignInAsync returning an IdentityUser object

        _jwtTokenGeneratorMock.Setup(generator =>
                generator.GenerateToken(It.IsAny<UserForLoginResponseDto>()))
            .Returns(expectedToken); // Simulating successful token generation

        _mapperMock.Setup(mapper => mapper.Map<UserForLoginResponseDto>(It.IsAny<IdentityUser>()))
            .Returns(new UserForLoginResponseDto()); // Simulating successful mapping
        // Act
        var result = await _authenticationServices.Login(loginQuery);

        Assert.IsNotNull(result);
        Assert.True(result.IsSuccess);
        Assert.That(result.Value.Token, Is.EqualTo(expectedToken)); // Check if token is generated successfully
        Assert.IsNotNull(result.Value.UserForLoginResponseDto); // Check if token is generated successfully
    }

    //reset password
    [Test]
    public async Task ResetPassword_ReturnsFailure_WhenUserNotFound()
    {
        // Arrange
        var changePasswordCommand = new ResetPasswordCommand("test@example.com", "123456", "newPassword");

        _identityDomainServicesMock.Setup(services =>
                services.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync((IdentityUser)null); // Simulating FindByEmailAsync returning null

        // Act
        var result = await _authenticationServices.ResetPassword(changePasswordCommand);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(AuthenticationErrorMessages.LoginFail));
    }

    [Test]
    public async Task ResetPassword_ReturnsFailure_WhenInvalidOtp()
    {
        // Arrange
        var changePasswordCommand = new ResetPasswordCommand("test@example.com", "123456", "newPassword");
        var mockIdentityUser = new IdentityUser("test@example.com", "testpassword");


        _identityDomainServicesMock.Setup(services =>
                services.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(mockIdentityUser); // Simulating FindByEmailAsync returning an IdentityUser object

        // Act
        var result = await _authenticationServices.ResetPassword(changePasswordCommand);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(AuthenticationErrorMessages.InvalidOtp));
    }

    [Test]
    public async Task ForgotPassword_ReturnsFailure_WhenUserNotFound()
    {
        // Arrange
        var email = "test@example.com";

        _identityDomainServicesMock.Setup(services =>
                services.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync((IdentityUser)null); // Simulating FindByEmailAsync returning null

        // Act
        var result = await _authenticationServices.ForgotPassword(email);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(AuthenticationErrorMessages.LoginFail));
    }

    [Test]
    public async Task ForgotPassword_SendsEmail_WhenUserFound()
    {
        // Arrange
        var email = "test@example.com";
        var mockIdentityUser = new IdentityUser(email, "testpassword");

        _identityDomainServicesMock.Setup(services =>
                services.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(mockIdentityUser);

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulating successful save changes

        // Act
        var result = await _authenticationServices.ForgotPassword(email);

        // Assert
        Assert.True(result.IsSuccess);
        // Verify that SendEmail method of the EmailSender mock was called once
        _emailSenderMock.Verify(e => e.SendEmail(email, It.IsAny<string>(), It.IsAny<string>(), false), Times.Once);
    }

    [Test]
    public async Task Register_ReturnsSuccess_WhenRegistrationSuccessful()
    {
        // Arrange
        var registerCommand = new RegisterCommand("Bob", "Bob@gmail.com", "123456", "123456", "123456", "123456");

        var expectedUser = new IdentityUser(registerCommand.PhoneNumber, registerCommand.Password);

        _identityDomainServicesMock.Setup(services =>
                services.CreateAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()
                ))
            .ReturnsAsync(new Result<IdentityUser>
            {
                IsSuccess = true,
                Value = expectedUser
            });

        _mapperMock.Setup(ma => ma.Map<UserForLoginResponseDto>(It.IsAny<IdentityUser>()))
            .Returns(new UserForLoginResponseDto());
        
        _jwtTokenGeneratorMock.Setup(generator =>
                generator.GenerateToken(It.IsAny<UserForLoginResponseDto>()))
            .Returns("test_token");
        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulating successful save changes

        // Act
        var result = await _authenticationServices.Register(registerCommand);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.NotNull(result.Value);
    }
    
    [Test]
    public async Task Register_ReturnsFail_WhenMapperThrowException()
    {
        // Arrange
        var registerCommand = new RegisterCommand("Bob", "Bob@gmail.com", "123456", "123456", "123456", "123456");

        var expectedUser = new IdentityUser(registerCommand.PhoneNumber, registerCommand.Password);

        _identityDomainServicesMock.Setup(services =>
                services.CreateAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()
                ))
            .ReturnsAsync(new Result<IdentityUser>
            {
                Value = expectedUser
            });

        _mapperMock.Setup(ma => ma.Map<UserForLoginResponseDto>(It.IsAny<IdentityUser>()))
            .Throws(new Exception("Mapper throw exception"));

        _jwtTokenGeneratorMock.Setup(generator =>
                generator.GenerateToken(It.IsAny<UserForLoginResponseDto>()))
            .Returns("test_token");

        // Act
        var result = await _authenticationServices.Register(registerCommand);

        // Assert
        Assert.NotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.IsNotEmpty(result.DisplayMessage);
    }

    [Test]
    public async Task Register_ReturnsFailure_WhenRegistrationFails()
    {
        // Arrange
        var registerCommand = new RegisterCommand("Bob", "Bob@gmail.com", "123456", "123456", "123456", "123456");

        _identityDomainServicesMock.Setup(services =>
                services.CreateAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()
                ))
            .ReturnsAsync(new Result<IdentityUser>
            {
                DisplayMessage = "Registration failed" // Simulate registration failure
            });

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulating successful save changes

        // Act
        var result = await _authenticationServices.Register(registerCommand);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.Contains("Registration failed", result.ErrorMessages);
    }

    [Test]
    public async Task ValidateToken_ReturnsSuccess_WhenTokenIsValid()
    {
        // Arrange
        var validateTokenQuery = new ValidateTokenQuery("valid_token");

        _jwtTokenGeneratorMock.Setup(generator =>
                generator.ValidateToken(It.IsAny<string>()))
            .Returns(true); // Simulate a valid token

        // Act
        var result = await _authenticationServices.ValidateToken(validateTokenQuery);

        // Assert
        Assert.IsNotNull(result);
        Assert.True(result.IsSuccess);
    }

    [Test]
    public async Task ValidateToken_ReturnsFailure_WhenTokenIsInvalid()
    {
        // Arrange
        var validateTokenQuery = new ValidateTokenQuery("invalid_token");

        _jwtTokenGeneratorMock.Setup(generator =>
                generator.ValidateToken(It.IsAny<string>()))
            .Returns(false); // Simulate an invalid token

        // Act
        var result = await _authenticationServices.ValidateToken(validateTokenQuery);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.Contains(AuthenticationErrorMessages.InvalidToken, result.ErrorMessages);
    }
}