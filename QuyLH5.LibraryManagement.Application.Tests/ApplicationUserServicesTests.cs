using MapsterMapper;
using MockQueryable.Moq;
using Moq;
using QuyLH5.LibraryManagement.Application.Contracts.Commons.ErrorMessages;
using QuyLH5.LibraryManagement.Application.Contracts.Users;
using QuyLH5.LibraryManagement.Application.ServiceImpls;
using QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.Identity;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Paginations;
using QuyLH5.LibraryManagement.Test.Shared;

namespace QuyLH5.LibraryManagement.Application.Tests;

public class ApplicationUserServicesTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock = new();
    private readonly Mock<IIdentityDomainServices> _identityDomainServicesMock = new();
    private readonly Mock<IMapper> _mapperMock = new();
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new();
    private readonly Mock<IAppLogger<UserServices>> _loggerMock = new();

    private readonly UserServices _userServices;

    public ApplicationUserServicesTests()
    {
        _userServices = new UserServices(
            _mapperMock.Object,
            _loggerMock.Object,
            _unitOfWorkMock.Object,
            _userRepositoryMock.Object,
            _identityDomainServicesMock.Object
        );
    }

    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task GetUsers_WithValidFilter_ShouldReturnPagedResult()
    {
        // Arrange
        var userFilterParams = new PaginatedParams()
        {
            // Set your filter parameters here
        };

        var expectedUsers = TestShared.Users;
        var expectedUserDtos = TestShared.UserForListDtos;

        //2 - build mock by extension
        var mock = expectedUsers.AsQueryable().BuildMock();
        _userRepositoryMock.Setup(repo => repo.GetAll())
            .Returns(mock);

        _userRepositoryMock.Setup(repo => repo.CountAsync())
            .ReturnsAsync(expectedUsers.Count);

        _mapperMock.Setup(ma => ma.Map<List<UserForListDto>>(expectedUsers))
            .Returns(expectedUserDtos);

        // Act
        var result = await _userServices.GetUsers(userFilterParams);

        // Assert
        Assert.IsNotNull(result);
        // Add assertions for the result
    }

    [Test]
    public async Task GetUserByIdAsync_WithValidId_ShouldReturnUserForDetailDto()
    {
        // Arrange
        var expectedUser = TestShared.IdentityUsers[0];
        var expectedUserDto = TestShared.UserForDetailDtos[0];

        _identityDomainServicesMock.Setup(repo => repo.GetUserIdAsync(expectedUser.Id))
            .ReturnsAsync(expectedUser);

        _mapperMock.Setup(ma => ma.Map<UserForDetailDto>(expectedUser.User))
            .Returns(expectedUserDto);

        // Act
        var result = await _userServices.GetUserDetailByIdAsync(expectedUser.Id.Value);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
        Assert.That(expectedUserDto.PhoneNumber, Is.EqualTo(result.Value.PhoneNumber));
        Assert.That(expectedUserDto.Name, Is.EqualTo(result.Value.Name));
        Assert.That(expectedUserDto.BalanceAmount, Is.EqualTo(result.Value.BalanceAmount));
        Assert.That(expectedUserDto.Address, Is.EqualTo(result.Value.Address));
    }

    [Test]
    public async Task GetUserByIdAsync_WithInvalidId_ShouldReturnNotFoundResult()
    {
        // Arrange
        IdentityUser? expectedUser = null;
        IdentityGuid fakeId = IdentityGuid.Create();

        _identityDomainServicesMock.Setup(repo => repo.GetUserIdAsync(fakeId))
            .ReturnsAsync(expectedUser);
        
        // Act
        var result = await _userServices.GetUserDetailByIdAsync(fakeId.Value);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
    }

    [Test]
    public async Task UpsertUserAsync_WithNewUser_ShouldCreateAndReturnSuccess()
    {
        //Arrange
        User? nullUser = null;

        var expectedUser = TestShared.Users[0];
        var newUserUpsert = new UserForUpsertDto(expectedUser.Id.Value, "New User", "usa city", "Usa");

        _userRepositoryMock.Setup(repo => repo.GetByIdAsync(IdentityGuid.Create(newUserUpsert.Id)))
            .ReturnsAsync(nullUser);

        _mapperMock.Setup(ma => ma.Map<User>(newUserUpsert)).Returns(expectedUser);

        _userRepositoryMock.Setup(repo => repo.InsertAsync(expectedUser));

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        //Act
        var result = await _userServices.UpsertUserAsync(newUserUpsert);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
    }

    [Test]
    public async Task UpsertUserAsync_WithExistingUser_ShouldUpdateAndReturnSuccess()
    {
        //Arrange
        var expectedUser = TestShared.Users[0];

        var userToUpdate = new UserForUpsertDto(expectedUser.Id.Value, "New User after update", "usa city", "Usa");

        _userRepositoryMock.Setup(repo => repo.GetByIdAsync(IdentityGuid.Create(userToUpdate.Id)))
            .ReturnsAsync(expectedUser);

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        //Act
        var result = await _userServices.UpsertUserAsync(userToUpdate);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
    }

    [Test]
    public async Task DeleteUserAsync_WithValidId_ShouldDeleteAndReturnSuccess()
    {
        var expectedUser = TestShared.Users[0];

        _userRepositoryMock.Setup(repo => repo.DeleteByIdAsync(expectedUser.Id))
            .ReturnsAsync(true);

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        //Act
        var result = await _userServices.DeleteUserAsync(expectedUser.Id.Value);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
    }

    [Test]
    public async Task DeleteUserAsync_WithInvalidId_ShouldReturnNotFoundResult()
    {
        var expectedUserId = Guid.Empty;
        User? user = null;

        _userRepositoryMock.Setup(repo => repo.GetByIdAsync(IdentityGuid.Create(expectedUserId)))
            .ReturnsAsync(user);

        //Act
        var result = await _userServices.DeleteUserAsync(expectedUserId);

        //Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
    }

    [Test]
    public async Task DepositAsync_WithValidUserIdAndAmount_ReturnsSuccessResult()
    {
        // Arrange
        decimal amount = 100.00m; // Replace with a valid deposit amount

        var mockUser = TestShared.IdentityUsers[0]; // Create a mock User object for testing
        var userId = mockUser.Id.Value;

        _identityDomainServicesMock.Setup(service => service.GetUserIdAsync(It.IsAny<IdentityGuid>()))
            .ReturnsAsync(mockUser);
        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1);

        // Act
        var result = await _userServices.DepositAsync(userId, amount);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
        Assert.That(result.Value, Is.EqualTo(amount));
    }

    [Test]
    public async Task DepositAsync_WithInvalidUserId_ReturnsFailureResult()
    {
        // Arrange
        var mockUser = TestShared.IdentityUsers[0];
        var userId = mockUser.Id.Value;
        decimal amount = 100.00m; // Replace with a valid deposit amount

        _identityDomainServicesMock.Setup(service => service.GetUserIdAsync(It.IsAny<IdentityGuid>()))
            .ReturnsAsync((IdentityUser)null);

        // Mock UnitOfWork and Logger as needed for the DepositAsync method

        // Act
        var result = await _userServices.DepositAsync(userId, amount);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(UserErrorMessages.UserNotFound));
    }
}