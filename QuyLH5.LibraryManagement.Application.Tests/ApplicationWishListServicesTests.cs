using MapsterMapper;
using Moq;
using QuyLH5.LibraryManagement.Application.Contracts.Commons.ErrorMessages;
using QuyLH5.LibraryManagement.Application.Contracts.Users;
using QuyLH5.LibraryManagement.Application.ServiceImpls;
using QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Results;

namespace QuyLH5.LibraryManagement.Application.Tests;

public class ApplicationWishListServicesTests
{
    private readonly Mock<IBookDomainServices> _bookDomainServicesMock = new();
    private readonly Mock<IMapper> _mapperMock = new();
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new();
    private readonly Mock<IAppLogger<BasketServices>> _loggerMock = new();

    private WishListServices _wishListServices;

    public ApplicationWishListServicesTests()
    {
        _wishListServices = new WishListServices(
            _mapperMock.Object,
            _unitOfWorkMock.Object,
            _loggerMock.Object,
            _bookDomainServicesMock.Object
        );
    }

    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task AddFavouriteBook_ReturnsSuccess_WhenItemIsAddedToWishList()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.AddFavouriteBook(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Success()); // Simulate successful addition
        
        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulate successful save changes

        // Act
        var result = await _wishListServices.AddFavouriteBook(wishlistItem);

        // Assert
        Assert.IsNotNull(result);
        Assert.True(result.IsSuccess);
    }
    
    [Test]
    public async Task AddFavouriteBook_ShouldReturnFail_WhenSavingChangesFail()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.AddFavouriteBook(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Success()); // Simulate successful addition
        
        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulate successful save changes

        // Act
        var result = await _wishListServices.AddFavouriteBook(wishlistItem);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(UserErrorMessages.AddFavoriteBookFailWhileSavingChanges));
    }

    [Test]
    public async Task AddFavouriteBook_ReturnsFailure_WhenItemAdditionFails()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.AddFavouriteBook(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Fail("Failed to add item"));

        // Act
        var result = await _wishListServices.AddFavouriteBook(wishlistItem);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo("Failed to add item"));
    }

    [Test]
    public async Task RemoveFavouriteBook_ReturnsSuccess_WhenItemIsRemovedFromWishList()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.Empty, 1);

        _bookDomainServicesMock.Setup(service =>
                service.RemoveFavouriteBook(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Success()); // Simulate successful removal

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulate successful save changes
        // Act
        var result = await _wishListServices.RemoveFavouriteBook(wishlistItem);

        // Assert
        Assert.True(result.IsSuccess);
    }

    [Test]
    public async Task RemoveFavouriteBook_ReturnsFailure_WhenItemRemovalFails()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.RemoveFavouriteBook(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Fail("Failed to remove item")); // Simulate failure in removal


        // Act
        var result = await _wishListServices.RemoveFavouriteBook(wishlistItem);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo("Failed to remove item"));
    }
    
    [Test]
    public async Task AddItemToWishList_ReturnsSuccess_WhenItemIsAddedToWishList()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.AddItemToWishList(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Success()); // Simulate successful addition

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulate successful save changes
        
        // Act
        var result = await _wishListServices.AddItemToWishList(wishlistItem);

        // Assert
        Assert.True(result.IsSuccess);
    }
    

    [Test]
    public async Task AddItemToWishList_ReturnsFailure_WhenItemAdditionFails()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.AddItemToWishList(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Fail("Failed to add item"));

        // Act
        var result = await _wishListServices.AddItemToWishList(wishlistItem);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(UserErrorMessages.AddItemToWishListFail));
        Assert.That(result.ErrorMessages.Count, Is.GreaterThan(0));
    }
    
    [Test]
    public async Task AddItemToWishList_ReturnsFailure_WhenSavingChangesFails()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.AddItemToWishList(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Success());

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulate save changes failure
        
        // Act
        var result = await _wishListServices.AddItemToWishList(wishlistItem);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(UserErrorMessages.AddItemToWishListFailWhileSavingChanges));
    }

    [Test]
    public async Task RemoveItemFromWishList_ReturnsSuccess_WhenItemIsRemovedFromWishList()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.Empty, 1);

        _bookDomainServicesMock.Setup(service =>
                service.RemoveItemToWishList(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Success()); // Simulate successful removal

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulate successful save changes
        
        // Act
        var result = await _wishListServices.RemoveItemFromWishList(wishlistItem);

        // Assert
        Assert.True(result.IsSuccess);
    }

    [Test]
    public async Task RemoveItemFromWishList_ReturnsFailure_WhenItemRemovalFails()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.RemoveItemToWishList(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Fail("Failed to remove item")); // Simulate failure in removal

        // Act
        var result = await _wishListServices.RemoveItemFromWishList(wishlistItem);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(UserErrorMessages.RemoveItemFromWishListFail));
        Assert.That(result.ErrorMessages.Count, Is.GreaterThan(0));
    }
    [Test]
    public async Task RemoveItemFromWishList_ReturnsFailure_WhenSavingChangesFails()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.RemoveItemToWishList(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Success());

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulate save changes failure
        
        // Act
        var result = await _wishListServices.RemoveItemFromWishList(wishlistItem);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(UserErrorMessages.RemoveItemFromWishListFailWhileSavingChanges));
    }
    
    [Test]
    public async Task RemoveFavouriteBook_ReturnsFailure_WhenSavingChangesFails()
    {
        // Arrange
        var wishlistItem = new WishlistItemForAddRemoveDto(Guid.NewGuid(), 1);

        _bookDomainServicesMock.Setup(service =>
                service.RemoveFavouriteBook(It.IsAny<IdentityGuid>(), It.IsAny<int>()))
            .ReturnsAsync(Result.Success());

        _unitOfWorkMock.Setup(unitOfWork => unitOfWork.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulate save changes failure
        
        // Act
        var result = await _wishListServices.RemoveFavouriteBook(wishlistItem);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(UserErrorMessages.RemoveFavouriteBookFailWhileSavingChanges));
    }
}