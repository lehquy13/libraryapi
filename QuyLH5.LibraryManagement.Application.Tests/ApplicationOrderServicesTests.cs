using MapsterMapper;
using Moq;
using QuyLH5.LibraryManagement.Application.Contracts.Commons.ErrorMessages;
using QuyLH5.LibraryManagement.Application.Contracts.Interfaces;
using QuyLH5.LibraryManagement.Application.Contracts.Order;
using QuyLH5.LibraryManagement.Application.ServiceImpls;
using QuyLH5.LibraryManagement.Domain.DomainServices.Interfaces;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Shared.Enums;
using QuyLH5.LibraryManagement.Domain.Shared.Results;
using QuyLH5.LibraryManagement.Domain.Specifications.Orders;
using QuyLH5.LibraryManagement.Test.Shared;

namespace QuyLH5.LibraryManagement.Application.Tests;

public class ApplicationOrderServicesTests
{
    private readonly Mock<IOrderDomainServices> _orderDomainServicesMock = new();
    private readonly Mock<IEmailSender> _emailSenderMock = new();
    private readonly Mock<IOrderRepository> _orderRepositoryMock = new();
    private readonly Mock<IMapper> _mapperMock = new();
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new();
    private readonly Mock<IAppLogger<OrderServices>> _loggerMock = new();

    private OrderServices _orderServices;

    public ApplicationOrderServicesTests()
    {
        _orderServices = new OrderServices(
            _mapperMock.Object,
            _unitOfWorkMock.Object,
            _loggerMock.Object,
            _orderDomainServicesMock.Object,
            _orderRepositoryMock.Object,
            _emailSenderMock.Object
        );
    }

    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task CreateOrder_ReturnsSuccess_WhenOrderIsCreated()
    {
        // Arrange
        var userId = Guid.NewGuid();
        _orderDomainServicesMock.Setup(service =>
                service.CreateOrderAsync(It.IsAny<IdentityGuid>()))
            .ReturnsAsync(Result.Success()); // Simulate successful order creation

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulate successful save changes
        
        _mapperMock.Setup(mapper => mapper.Map<OrderForDetailDto>(It.IsAny<Order>()))
            .Returns(new OrderForDetailDto()); // Simulate successful mapping

        // Act
        var result = await _orderServices.CreateOrder(userId);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.That(result.Value, Is.Not.Null);
        Assert.IsInstanceOf<OrderForDetailDto>(result.Value);
    }

    [Test]
    public async Task CreateOrder_ReturnsFailure_WhenOrderCreationFails()
    {
        // Arrange
        var userId = Guid.NewGuid();

        _orderDomainServicesMock.Setup(service =>
                service.CreateOrderAsync(It.IsAny<IdentityGuid>()))
            .ReturnsAsync(Result.Fail("Failed to create order")); // Simulate failure in order creation

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulate save changes failure

        // Act
        var result = await _orderServices.CreateOrder(userId);

        // Assert
        Assert.False(result.IsSuccess);
        // Optionally check for the error message
        Assert.That(result.DisplayMessage, Is.EqualTo("Failed to create order"));
    }

    [Test]
    public async Task CreateOrder_ReturnsFailure_WhenSavingChangesFails()
    {
        // Arrange
        var userId = Guid.NewGuid();

        _orderDomainServicesMock.Setup(service =>
                service.CreateOrderAsync(It.IsAny<IdentityGuid>()))
            .ReturnsAsync(Result.Success()); // Simulate failure in order creation

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulate save changes failure

        // Act
        var result = await _orderServices.CreateOrder(userId);

        // Assert
        Assert.False(result.IsSuccess);
        // Optionally check for the error message
        Assert.That(result.DisplayMessage, Is.EqualTo(OrderErrorMessages.CreateOrderFailWhileSavingChanges));
    }


    [Test]
    public async Task GetAllOrders_WithValidParams_ReturnsPaginationResult()
    {
        // Arrange
        var paginatedParams = new OrderPaginatedParams
        {
            PageIndex = 1,
            PageSize = 10,
            UserId = Guid.NewGuid()
        };

        var mockOrders = new List<Order>(); // Replace with mock orders for testing
        _orderRepositoryMock.Setup(repo => repo.CountAsync(It.IsAny<OrderListQuerySpec>()))
            .ReturnsAsync(mockOrders.Count);
        _orderRepositoryMock.Setup(repo => repo.GetAllListAsync(It.IsAny<OrderListQuerySpec>()))
            .ReturnsAsync(mockOrders);

        // Act
        var result = await _orderServices.GetAllOrders(paginatedParams);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsInstanceOf<PaginationResult<OrderForDetailDto>>(result);
    }

    [Test]
    public async Task PurchaseOrder_WithValidData_ReturnsSuccessResult()
    {
        // Arrange
        var customerId = Guid.NewGuid();
        var orderId = Guid.NewGuid();
        var paymentMethod = "Card";
        var expectedResult = Result<Order>.Success(TestShared.Orders[0]);

        _orderDomainServicesMock
            .Setup(services =>
                services.PurchaseOrder(It.IsAny<IdentityGuid>(), It.IsAny<OrderGuid>(), It.IsAny<PaymentMethod>()))
            .ReturnsAsync(expectedResult);

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None)).ReturnsAsync(1);

        _emailSenderMock.Setup(sender =>
            sender.SendEmail(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                false
            )).Returns(Task.CompletedTask);

        // Act
        var result = await _orderServices.PurchaseOrder(customerId, orderId, paymentMethod);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
        // Assert
        _emailSenderMock.Verify(sender => sender.SendEmail(
            It.IsAny<string>(),
            It.IsAny<string>(),
            It.IsAny<string>(),
            false
        ), Times.Once);
    }

    [Test]
    public async Task PurchaseOrder_ReturnsFailure_WhenSaveChangesFails()
    {
        // Arrange
        var customerId = Guid.NewGuid();
        var orderId = Guid.NewGuid();
        var paymentMethod = "Card"; // or any valid PaymentMethod value

        _orderDomainServicesMock.Setup(service =>
                service.PurchaseOrder(It.IsAny<IdentityGuid>(), It.IsAny<OrderGuid>(), It.IsAny<PaymentMethod>()))
            .ReturnsAsync(Result<Order>.Success(TestShared.Orders[0])); // Simulate successful order purchase

        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(0); // Simulate failure in save changes

        // Act
        var result = await _orderServices.PurchaseOrder(customerId, orderId, paymentMethod);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(OrderErrorMessages.PurchaseOrderFailWhileSavingChanges));

        // Verify that SendEmail method was not called
        _emailSenderMock.Verify(sender => sender.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),false),
            Times.Never);
    }

    [Test]
    public async Task PurchaseOrder_ReturnsFailure_WhenOrderServiceFails()
    {
        // Arrange
        var customerId = Guid.NewGuid();
        var orderId = Guid.NewGuid();
        var paymentMethod = "Card"; // or any valid PaymentMethod value


        _orderDomainServicesMock.Setup(service =>
                service.PurchaseOrder(It.IsAny<IdentityGuid>(), It.IsAny<OrderGuid>(), It.IsAny<PaymentMethod>()))
            .ReturnsAsync(Result.Fail("Failed to purchase order")); // Simulate failure in purchasing order


        _unitOfWorkMock.Setup(uow => uow.SaveChangesAsync(CancellationToken.None))
            .ReturnsAsync(1); // Simulate successful save changes


        // Act
        var result = await _orderServices.PurchaseOrder(customerId, orderId, paymentMethod);

        // Assert
        Assert.IsNotNull(result);
        Assert.False(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo("Failed to purchase order"));

        // Verify that SendEmail method was not called
        _emailSenderMock.Verify(
            sender => sender.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), false),
            Times.Never);
    }

    [Test]
    public async Task GetOrder_WithValidGuid_ReturnsOrderForDetailDto()
    {
        // Arrange
        var orderGuid = Guid.NewGuid();
        var order = TestShared.Orders[0];
        var expectedDto = new OrderForDetailDto
        {
            /* Create a mock OrderForDetailDto object */
        }; // Replace with expected OrderForDetailDto

        _orderRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<OrderGuid>())).ReturnsAsync(order);
        _mapperMock.Setup(mapper => mapper.Map<OrderForDetailDto>(order)).Returns(expectedDto);

        // Act
        var result = await _orderServices.GetOrder(orderGuid);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsTrue(result.IsSuccess);
        Assert.That(result.Value, Is.EqualTo(expectedDto));
    }

    [Test]
    public async Task GetOrder_WithValidGuid_ReturnsFail()
    {
        // Arrange
        var orderGuid = Guid.NewGuid();
        Order? order = null;

        _orderRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<OrderGuid>())).ReturnsAsync(order);

        // Act
        var result = await _orderServices.GetOrder(orderGuid);

        // Assert
        Assert.IsNotNull(result);
        Assert.IsFalse(result.IsSuccess);
        Assert.That(result.DisplayMessage, Is.EqualTo(OrderErrorMessages.OrderNotFound));
    }
}