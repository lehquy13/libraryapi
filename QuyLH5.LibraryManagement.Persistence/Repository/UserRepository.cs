﻿using Microsoft.EntityFrameworkCore;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate;
using QuyLH5.LibraryManagement.Domain.Library.UserAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Persistence.Entity_Framework_Core;

namespace QuyLH5.LibraryManagement.Persistence.Repository;

public class UserRepository : RepositoryImpl<User, IdentityGuid>, IUserRepository
{
    public UserRepository(AppDbContext appDbContext, IAppLogger<UserRepository> logger) : base(appDbContext, logger)
    {
    }

    public async Task<User?> GetFullById(IdentityGuid id)
    {
        try
        {
            var fullUser = await AppDbContext
                .Users
                .Include(x => x.FavouriteBooks)
                .ThenInclude(x => x.Authors)
                .Include(x => x.WishLists)
                .ThenInclude(x => x.Authors)
                .FirstOrDefaultAsync(x => x.Id == id);
            return fullUser;
        }
        catch (Exception ex)
        {
            Logger.LogError(ErrorMessage, "GetFullById", ex.Message);
            return null;
        }
    }
}