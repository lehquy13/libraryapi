﻿using Microsoft.EntityFrameworkCore;
using QuyLH5.LibraryManagement.Domain.Interfaces;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate;
using QuyLH5.LibraryManagement.Domain.Library.OrderAggregate.ValueObjects;
using QuyLH5.LibraryManagement.Domain.Specifications.Orders;
using QuyLH5.LibraryManagement.Persistence.Entity_Framework_Core;

namespace QuyLH5.LibraryManagement.Persistence.Repository;

public class OrderRepository : RepositoryImpl<Order, OrderGuid>, IOrderRepository
{
    public OrderRepository(AppDbContext appDbContext, IAppLogger<OrderRepository> logger) : base(appDbContext, logger)
    {
    }

    public async Task<List<Order>> GetAllListAsync(OrderListQuerySpec orderListQuerySpec)
    {
        try
        {
            var result = GetQuery(AppDbContext.Set<Order>(), orderListQuerySpec);

            return await result.AsNoTracking().ToListAsync();
        }
        catch (Exception ex)
        {
            Logger.LogError(ErrorMessage, "GetAllListAsync", ex.Message);
            return new();
        }
    }
}